{- Homework solution setup -}
{-# OPTIONS_GHC -Wall #-}

module LogAnalysis where

import Log
import Text.Read (readMaybe)

{- Exercise 1: -}

{- a) Goal is to parse a single message.

    For example:
      >>> parseMessage "E 2 562 help help" == LogMessage (Error 2) 562 "help help"
      True
      >>> parseMessage "I 29 la la la" == LogMessage Info 29 "la la la"
      True
-}

parseUncertainNumericMessageParts :: (Int -> b) -> ([String] -> c) -> [String] -> Maybe (b, c)
parseUncertainNumericMessageParts _ _ [] = Nothing
parseUncertainNumericMessageParts numF msgF (x : msg) = case readMaybe x of
  Just n -> Just (numF n, msgF msg)
  Nothing -> Nothing

parseMessageContent :: [String] -> Maybe (MessageType, Maybe (TimeStamp, String))
parseMessageContent [] = Nothing
parseMessageContent (x : xs)
  | x == "I" = Just (Info, parseMessageTimestampBody xs)
  | x == "W" = Just (Warning, parseMessageTimestampBody xs)
  | x == "E" = parseUncertainNumericMessageParts Error parseMessageTimestampBody xs
  | otherwise = Nothing
  where
    parseMessageTimestampBody = parseUncertainNumericMessageParts id unwords

parseMessage :: String -> LogMessage
parseMessage xs = case parseMessageContent (words xs) of
  Nothing -> Unknown ""
  Just (_, Nothing) -> Unknown ""
  Just (msgType, Just (tmStmp, msgBody)) -> LogMessage msgType tmStmp msgBody

{- b) Goal is to parse the whole log file.
  Define a function
  parse :: String -> [LogMessage]
  which parses an entire log file at once and returns its contents as a
  list of LogMessages.
 -}

parse :: String -> [LogMessage]
parse = map parseMessage . lines

{- Exercise 2:

  Goal is to implement a function `insert` which inserts
  a new LogMessage into an existing MessageTree, producing
  a new MessageTree.

  A MessageTree should be sorted by timestamp: that is,
  the timestamp of a LogMessage in any Node should be greater
  than all timestamps of any LogMessage in the left subtree,
  and less than all timestamps of any LogMessage in the right child.
-}

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) mtree = mtree
insert msg Leaf = Node Leaf msg Leaf
insert msg (Node left (Unknown _) right) = Node left msg right
insert msg@(LogMessage _ t _) (Node left root@(LogMessage _ t2 _) right)
  | t < t2 = Node (insert msg left) root right
  | t >= t2 = Node left root (insert msg right)

{- Exercise 3:

  Goal is to build a complete MessageTree from a list of messages.
-}

build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

{- Exercise 4:

  Goal is to take a sorted MessageTree and produces a list of all the
  LogMessages it contains, sorted by timestamp from smallest to biggest.
-}

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node left msg right) = inOrder left ++ [msg] ++ inOrder right

{- Exercise 5:

  Goal is to get the list of log messages that only contain
  "errors with a severity of at least 50"
-}

iSevereLogMessage :: Int -> LogMessage -> Bool
iSevereLogMessage threshold (LogMessage (Error severity) _ _) = severity > threshold
iSevereLogMessage _ _ = False

getContent :: LogMessage -> String
getContent (LogMessage _ _ s) = s
getContent (Unknown s) = s

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map getContent . filter (iSevereLogMessage 50) . inOrder . build