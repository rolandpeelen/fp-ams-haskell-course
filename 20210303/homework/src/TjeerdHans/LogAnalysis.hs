{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module LogAnalysis where

import Log
import Data.Maybe
import Text.Read

-- exercise 1
parseMessage :: String -> LogMessage
parseMessage [] = Unknown "Empty message."
parseMessage xs 
    | messageType == "I" = parseMessageContent Info (tail msgWords)
    | messageType == "W" = parseMessageContent Warning (tail msgWords)
    | messageType == "E" = parseMessageError (tail msgWords)
    | otherwise = Unknown "Unknown message type."
    where msgWords = words xs
          messageType = head msgWords
          

parseMessageContent :: MessageType -> [String] -> LogMessage 
parseMessageContent _ [] = Unknown "Empty message content."
parseMessageContent messageType xs
    | isJust timeStamp =  LogMessage messageType (fromJust timeStamp) (unwords $ tail xs)
    | otherwise = Unknown "Message has no valid timestamp."
    where timeStamp = readMaybe (head xs) :: Maybe TimeStamp 

parseMessageError :: [String] -> LogMessage 
parseMessageError [] = Unknown "Empty message content."
parseMessageError xs
    | isJust severity = parseMessageContent (Error (fromJust severity)) (tail xs)
    | otherwise = Unknown "Error message has no valid severity."
    where severity = readMaybe (head xs) :: Maybe Int 

parse :: String -> [LogMessage]
parse [] = []
parse xs  = [parseMessage x | x <- lines xs]


-- exercise 2
insert :: LogMessage -> MessageTree -> MessageTree 
insert (Unknown _) t = t
insert m Leaf = Node Leaf m Leaf 
insert msg@(LogMessage _ nt _) tree@(Node l lg@(LogMessage _ t _) r) 
    | nt < t = Node (insert msg l) lg r
    | nt > t = Node l lg (insert msg r)
    | otherwise = tree -- same timestamps, ignore? IRL timestamps won't collide


-- exercise 3
build :: [LogMessage] -> MessageTree 
build = foldr insert Leaf -- \acc x -> insert x acc
-- build = foldl (flip insert) Leaf -- \acc x -> insert x acc

-- exercise 4
inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node l m r) = inOrder l ++ [m] ++ inOrder r

-- exercise 5
messagesOrdered :: [LogMessage] -> [LogMessage]                 
messagesOrdered = inOrder . build 

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong xs = [m | (LogMessage (Error s) _ m)<-messagesOrdered xs, s >=50] 

-- oh noes, it's the mustardwatch! We're out of mustard!

-- exercise 6
getMessage :: LogMessage -> String
getMessage (LogMessage _ _ m) = m

getMessageByIndex :: [(a, LogMessage)] -> Int -> String
getMessageByIndex xs i = getMessage . snd $ xs!!i
inspectSurroundingMessages :: [LogMessage] -> [String]
inspectSurroundingMessages xs = concat [ [getMessageByIndex ms (i-2),getMessageByIndex ms (i-1)] | (i,LogMessage (Error s) _ _)<-ms, s>=50]
    where ms = zip [0..] (messagesOrdered xs)

