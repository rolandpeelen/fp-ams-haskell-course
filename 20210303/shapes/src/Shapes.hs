module Shapes
  ( Point (..),
    Shape (..),
    baseCircle,
    baseRect,
    surface,
    move,
  )
where

data Point = Point Float Float
  deriving (Show)

data Shape
  = Circle Point Float
  | Rectangle Point Point
  deriving (Show)

baseCircle :: Float -> Shape
baseCircle r = Circle (Point 0 0) r

baseRect :: Float -> Float -> Shape
baseRect width height = Rectangle (Point 0 0) (Point width height)

surface :: Shape -> Float
surface (Circle _ r) = pi * r ^ 2
surface (Rectangle (Point x1 y1) (Point x2 y2)) =
  abs (x2 - x1) * abs (y2 - y1)

move :: Shape -> Float -> Float -> Shape
move (Circle c r) dx dy =
  Circle (move' c dx dy) r
move (Rectangle p1 p2) dx dy =
  Rectangle (move' p1 dx dy) (move' p2 dx dy)

move' :: Point -> Float -> Float -> Point
move' (Point x y) dx dy = Point (x + dx) (y + dy)
