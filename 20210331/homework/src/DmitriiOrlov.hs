{- Homework Week 8:
    https://www.cis.upenn.edu/~cis194/spring13/hw/10-applicative.pdf
 -}

module DmitriiOrlov where

import Data.Char
import Data.Maybe

-- Parseer Introduction

newtype Parser a = Parser {runParser :: String -> Maybe (a, String)}

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
  where
    f [] = Nothing -- fail on the empty input
    f (x : xs) -- check ift x saisfies the predicate
    -- if so, return x along with the remainder
    -- of the input (that is, xs)
      | p x = Just (x, xs)
      | otherwise = Nothing -- otherwise, fail

char :: Char -> Parser Char
char c = satisfy (== c)

-- Then runnning the below should give the expected result Just (’A’,"BC"):
-- >>> runParser (satisfy isUpper) "ABC"
-- Just ('A',"BC")

posInt :: Parser Integer
posInt = Parser f
  where
    f xs
      | null ns = Nothing
      | otherwise = Just (read ns, rest)
      where
        (ns, rest) = span isDigit xs

-- Exercise start

{- Exercise 1:
    Implement a Functor instance for Parser.
    Hint: You may find it useful to implement a function
 -}

first :: (a -> b) -> (a, c) -> (b, c)
first f (x, y) = (f x, y)

-- Instance of Functor for Parser should be such that
--  Parser f => fmap :: (a -> b) -> f a -> f b
-- Therefore, we can notice that to keep the structure we need to
-- first run given parser (argument `f a`), apply `(a -> b)` over Maybe to
-- the result of that parser and then, based on the result of that application,
-- construct the new parser `p2 :: Parser b`.

instance Functor Parser where
  fmap f p1 = Parser (fmap (first f) <$> runParser p1)

{- Exercise 2:
    Implement an Applicative instance for Parser.
      Tips:
        - `pure a :: Parser a => a -> f a` represents the parser which
          consumes no input and successfully returns a result of a.
        - `p1 <*> p2` is a "piping" application of first p1 and then p2.
          if either p1 or p2 fails to parse, then the result should fail.
 -}

-- Given for example

type Name = String

data Employee = Emp {name :: Name, phone :: String}

-- Then

-- parseName :: Parser Name
-- parsePhone :: Parser String

-- Emp <$> parseName <*> parsePhone :: Parser Employee

-- Instance of an Applicative Parser should be such that
--  `pure` is an "empty" parser, returning the most basic applicative parser.

instance Applicative Parser where
  pure x = Parser (\input -> Just (x, input))
  (<*>) p1 p2 = Parser f
    where
      f input = case runParser p1 input of
        Nothing -> Nothing
        Just (x, remainder) -> runParser (fmap x p2) remainder

{- Exercise 3:
    Make use of the prepared Applicative and Functor Parsers.
 -}

-- 3.a:
-- >>> runParser abParser "abcdef"
-- Just ((’a’,’b’),"cdef")

-- NB: using `char` as defined in Exercise 1
abParser :: Parser (Char, Char)
abParser = (,) <$> char 'a' <*> char 'b'

-- 3.b:
-- >>> runParser abParser_ "abcdef"
-- Just ((),"cdef")

abParser_ :: Parser ()
-- first implemented as:
-- abParser_ = const () <$> abParser
-- but HLS suggests to use `() <$ ...` instead
abParser_ = () <$ abParser

-- 3.c:
-- >>> runParser intPair "12 34"
-- Just ([12,34],"")
-- Tip: use `posInt` to parse integer values, see "Introduction" section

intPair :: Parser [Integer]
intPair = (\x _ y -> [x, y]) <$> posInt <*> char ' ' <*> posInt

-- extra, to cope with possibly larger inputs generically
intListParser :: Parser [Integer]
intListParser = (: []) <$> posInt

spaceListParser :: Parser [a]
spaceListParser = [] <$ char ' '

intPair' = (++) <$> intListParser <*> ((++) <$> spaceListParser <*> intListParser)

-- looking at the pattern it seems very similar to `foldr`

intPair'' :: Parser [Integer]
intPair'' = foldr (\f g -> (++) <$> f <*> g) intListParser [intListParser, spaceListParser]

-- this would allow, to quickly implement `intTriple :: Parser [Integer]`
-- such that >>> runParser intTriple "12 34 45"
-- returns   Just ([12,34,45],"")
-- as
intTriple :: Parser [Integer]
intTriple = foldr (\f g -> (++) <$> f <*> g) intListParser (parsers ++ parsers)
  where
    parsers = [intListParser, spaceListParser]

{- Exercise 4:
    Write an Alternative instance for Parser allowing to implement "or" matching
 -}

class Applicative f => Alternative f where
  --  empty should be the identity element for the Alternative
  -- (<|>), and often represents failure.
  empty :: f a

  -- (<|>) is intended to represent choice: that is, f1 <|> f2 represents
  -- a choice between f1 and f2.
  (<|>) :: f a -> f a -> f a

-- Needed to be implemented to allow <|> to be applied to Maybe
instance Alternative Maybe where
  empty = Nothing
  (Just x) <|> _ = Just x
  Nothing <|> x = x

instance Alternative Parser where
  empty = Parser (const Nothing)
  p1 <|> p2 = Parser (\input -> runParser p1 input <|> runParser p2 input)

{- Exercise 5:
    Implement `intOrUppercase :: Parser ()` which parses either
    an integer value or an uppercase character, and fails otherwise.
 -}

-- Examples:
-- >>> runParser intOrUppercase "342abcd"
-- Just ((), "abcd")
-- >>> runParser intOrUppercase "XYZ"
-- Just ((), "YZ")
-- >>> runParser intOrUppercase "foo"
-- Nothing

intOrUppercase :: Parser ()
intOrUppercase = (() <$ posInt) <|> (() <$ satisfy isUpper)