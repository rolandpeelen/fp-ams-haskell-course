{- CIS 194 HW 10
   due Monday, 1 April
-}

module AParser where

import Control.Applicative

import Data.Char

-- A parser for a value of type a is a function which takes a String
-- representing the input to be parsed, and succeeds or fails; if it
-- succeeds, it returns the parsed value along with the remainder of
-- the input.
newtype Parser a = Parser { runParser :: String -> Maybe (a, String) }

-- For example, 'satisfy' takes a predicate on Char, and constructs a
-- parser which succeeds only if it sees a Char that satisfies the
-- predicate (which it then returns).  If it encounters a Char that
-- does not satisfy the predicate (or an empty input), it fails.
satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
  where
    f [] = Nothing    -- fail on the empty input
    f (x:xs)          -- check if x satisfies the predicate
                        -- if so, return x along with the remainder
                        -- of the input (that is, xs)
        | p x       = Just (x, xs)
        | otherwise = Nothing  -- otherwise, fail

-- Using satisfy, we can define the parser 'char c' which expects to
-- see exactly the character c, and fails otherwise.
char :: Char -> Parser Char
char c = satisfy (== c)

{- For example:

*Parser> runParser (satisfy isUpper) "ABC"
Just ('A',"BC")
*Parser> runParser (satisfy isUpper) "abc"
Nothing
*Parser> runParser (char 'x') "xyz"
Just ('x',"yz")

-}

-- For convenience, we've also provided a parser for positive
-- integers.
posInt :: Parser Integer
posInt = Parser f
  where
    f xs
      | null ns   = Nothing
      | otherwise = Just (read ns, rest)
      where (ns, rest) = span isDigit xs

------------------------------------------------------------
-- Your code goes below here
------------------------------------------------------------
-- Exercise 1
-- apply a function on the first element of a pair
first :: (a -> b) -> (a, c) -> (b, c)
first f (x, y) = (f x, y)


instance Functor Parser where
    fmap f p = Parser $ \s -> first f <$> runParser p s

-- Exercise 2

instance Applicative Parser where
-- pure :: a -> Parser a
    pure x = Parser (\s -> Just (x, s))
-- (<*>) :: Parser (a -> b) -> Parser a -> Parser b
    (<*>) p1 p2 = Parser (\s -> case runParser p1 s of
                            Nothing -> Nothing
                            Just (g, res) -> runParser (fmap g p2) res)

-- Exercise 3

--Create a parser
-- abParser :: Parser (Char, Char) which expects to see the characters ’a’ and ’b’ and returns them
-- as a pair. That is,
--
--   *AParser> runParser abParser "abcdef"
--   Just ((’a’,’b’),"cdef")
--   *AParser> runParser abParser "aebcdf"
--   Nothing
-- abParser expects to see the characters ’a’ and ’b’ and returns them as a pair. 

abParser :: Parser (Char, Char)
abParser = pure g <*> char 'a' <*> char 'b'
  where g x y = (x, y)

-- abParser_ acts in the same way as abParser but returns () instead of the characters ’a’ and ’b’.

abParser_ :: Parser ()
abParser_ = pure g <*> char 'a' <*> char 'b'
  where g _ _ = ()

-- 
-- Create a parser intPair which reads two integer values separated by a space and returns the integer values in a list.
-- You should use the provided posInt to parse the integer values.
--   *Parser> runParser intPair "12 34"
--   Just ([12,34],"")
-- 

intPair :: Parser [Integer]
intPair = pure g <*> posInt <*> char ' ' <*> posInt
  where g x y z = [x, z]

-- Exercise 4

-- class Applicative f => Alternative f where
-- empty :: f a
-- (<|>) :: f a -> f a -> f a

instance Alternative Parser where
--  empty :: Parser a    
    empty = Parser (\s -> Nothing)
    (<|>) f g = Parser (\s -> case runParser f s of
                  Nothing -> runParser g s
                  Just (x, rest) -> Just (x, rest))

-- Exercise 5

-- Implement a parser intOrUppercase :: Parser () which
--   parses either an integer value or an uppercase character, and fails otherwise.
--
-- *Parser> runParser intOrUppercase "342abcd"
-- Just ((), "abcd")
-- *Parser> runParser intOrUppercase "XYZ"
-- Just ((), "YZ")
-- *Parser> runParser intOrUppercase "foo"
-- Nothing
--

-- intOrUppercase :: Parser ()
-- intOrUppercase = pure g <*> (\s -> posInt) <|> (\s -> satisfy isUpper)
--   where g s = Just ((), s)