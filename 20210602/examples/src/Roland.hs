module Roland where

import Tree
import Prelude hiding (concat, length, (++))

type State s a = s -> (a, s)

------------------------------
-- Rewrite the definitions of pure and next to work with an arbitrary stateful
-- computation State s a. Hint: you only need to change the type signatures.type
------------------------------

-- Also rewrote to where syntax as I find it slightly more readable
next :: State s a -> (a -> State s b) -> State s b
next f g i = g x i
  where
    (x, i') = f i

-- Original syntax (with lamda hlint hint fixed);
next' :: State s a -> (a -> State s b) -> State s b
next' f g i = let (x, i') = f i in g x i

------------------------------
-- Write a function (++) that takes two lists and returns its concatenation.a
------------------------------
(++) :: [a] -> [a] -> [a]
(++) [] ys = ys
(++) (x : xs) ys = x : xs ++ ys

concat :: [[a]] -> [a]
concat = foldr (++) []

------------------------------
-- Define map and singleton
------------------------------
-- Map might as well be fmap
-- Repl: a = (Node (Leaf 1) (Leaf 1))
-- (fmap (+2) a) == Node (Leaf 3) (Leaf 3)
instance Functor Tree where
  fmap fn (Leaf l) = Leaf $ fn l
  fmap fn (Node l r) = Node (fn <$> l) (fn <$> r)

singleton :: a -> Tree a
singleton = Leaf

-- Hmmm apply a tree of functions to a tree of functions?
-- Is this even a thing?
instance Applicative Tree where
  pure = singleton
  (<*>) = undefined
