{-# OPTIONS_GHC -Wall -Werror #-}

module Tree where

data Tree a
  = Leaf a
  | Node (Tree a) (Tree a) deriving Show

numberOfLeaves :: Tree a -> Integer
numberOfLeaves (Leaf _) = 1
numberOfLeaves (Node l r) = numberOfLeaves l + numberOfLeaves r
