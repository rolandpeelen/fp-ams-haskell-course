# Haskell: From Beginner to Intermediate #14

## Monads Part 1: Discovering Monads

### FP AMS 02/06/2021

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. **Monads**
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline](book-of-monads.png)

---

# Functor and Applicative Functor

```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b
  
class Functor f => Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b
  liftA2 :: (a -> b -> c) -> f a -> f b -> f c
```

---

# State Contexts

## Binary Tree

```haskell
data Tree a
  = Leaf a
  | Node (Tree a) (Tree a)
```

## Counting the Number of Leaves

```haskell
numberOfLeaves :: Tree a -> Integer
numberOfLeaves (Leaf _) = 1
numberOfLeaves (Node l r) = numberOfLeaves l + numberOfLeaves r
```

---

# State Contexts

## Relabeling the Leaves

> If we start with a tree `t`, the result of `relabel` should contain the same elements, but with each one paired with the index it would receive if the leaves of `t` were flattened into a list starting with the leftmost leaf and ending with the rightmost leave.

```haskell
> relabel $
    Node (Leaf 'a')
         (Node (Leaf 'b')
               (Leaf 'c'))

Node (Leaf (1, 'a'))
     (Node (Leaf (2, 'b'))
           (Leaf (3, 'c')))
```

---

# State Contexts

## Relabeling the Leaves

```haskell
relabel :: Tree a -> Tree (Int, a)
relabel (Leaf x) = Leaf (???, x)
relabel (Node l r) = Node (relabel l) (relabel r)
```

```haskell
relabel :: Tree a -> Int -> (Tree (Int, a), Int)
relabel (Leaf x) i = (Leaf (i, x), i+1)
relabel (Node l r) i =
  let (l', i1) = relabel l i
      (r', i2) = relabel r i1
   in (Node l' r', i2)
```

---

# State Contexts

## Relabeling the Leaves

```haskell
type WithCounter a = Int -> (a, Int)

relabel :: Tree a -> WithCounter (Tree a)

relabel (Leaf x) i = (Leaf (i, x), i+1)
relabel (Node l r) i =
  let (l', i1) = relabel l i
      (r', i2) = relabel r i1
   in (Node l' r', i2)
```

---

# State Contexts

## Relabeling the Leaves

```haskell
type WithCounter a = Int -> (a, Int)

relabel (Node l r) i =
  let (l', i1) = relabel l i
      (r', i2) = relabel r i1
   in (Node l' r', i2)
   
let (x, newCounter) = … oldCounter
 in nextAction … x … newCounter
 
next :: WithCounter a -> (a -> WithCounter b) -> WithCounter b
f `next` g = \i -> let (x, i') = f i in g x i'

pure :: a -> WithCounter a
pure x = \i -> (x, i) 
```

---

# State Contexts

## Relabeling the Leaves

```haskell
type WithCounter a = Int -> (a, Int)

next :: WithCounter a -> (a -> WithCounter b) -> WithCounter b
f `next` g = \i -> let (x, i') = f i in g r i'

pure :: a -> WithCounter a
pure x = \i -> (x, i) 

relabel ( Leaf x) = \i -> (Leaf (i,x), i + 1)
relabel ( Node l r) = relabel l `next` \l' -> 
                      relabel r `next` \r' -> 
                      pure (Node l' r') 
```

---

# State Contexts

```haskell
type WithCounter a = Int -> (a, Int)

type State s a = s -> (s,a)
```

## Homework

Rewrite the definitions of `pure` and `next` to work with an arbitrary stateful 
computation `State s a`. Hint: you only need to change the type signatures. 

---

# Lists

```haskell
data [a] = [] | a : [a] 

length :: [a] -> Integer 
length [] = 0 
length (_ : xs) = 1 + length xs 
```

## Homework

Write a function `(++)` that takes two lists and returns its concatenation.

---

# Lists
 
```haskell
map :: (a -> b) -> [a] -> [b] 

singleton :: a -> [a]

concat :: [[a]] -> [a]
concat [] = []
concat (x : xs) = x ++ concat xs

-- alternatively 
concat = foldr ( ++ ) [] 
```

## Homework

Define `map` and `singleton`.
